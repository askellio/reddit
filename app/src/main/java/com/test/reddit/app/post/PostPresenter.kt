package com.test.reddit.app.post

import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.test.reddit.R
import com.test.reddit.RedditApp
import com.test.reddit.app.base.BasePresenter
import com.test.reddit.domain.interceptor.PostInterceptor
import com.test.reddit.domain.model.Post
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiConsumer
import java.util.*
import javax.inject.Inject

class PostPresenter:
    BasePresenter<PostView>() {

    @Inject lateinit var postInterceptor: PostInterceptor
    @Inject lateinit var connectivityManager: ConnectivityManager

    private var mDisposable: Disposable? = null
    private var msgRes: Int = 0
    private var posts: List<Post> = listOf()

    private lateinit var mMode: PostView.ViewMode

    init {
        RedditApp.appComponent.inject (this)
    }

    override fun doOnFirstRun() = load()

    override fun doOnOtherRun() {
        when (mMode) {
            PostView.ViewMode.MAIN -> view.showData (posts)
            PostView.ViewMode.MSG -> view?.showMessage (msgRes)
        }
        view?.switchMode (mMode)
    }

    fun onRetry () = load()
    fun onUpdate () = load()

    private fun load() {
        mMode = PostView.ViewMode.LOADING
        view?.switchMode(mMode)

        dispose (mDisposable)

        val connected: Boolean
        val info:NetworkInfo? = connectivityManager.getActiveNetworkInfo()
        if (info == null)
            connected = false
        else
            connected = info.isConnected
        mDisposable = postInterceptor.posts(connected).subscribe (
             { posts -> run {
                 if (posts.isEmpty()) {
                     mMode = PostView.ViewMode.MSG
                     msgRes = R.string.no_data
                     view?.showMessage (msgRes)
                 } else {
                     mMode = PostView.ViewMode.MAIN
                     this.posts = posts
                     view?.showData (this.posts)
                 }
                 view?.switchMode (mMode)
             } }, { t -> run {
                mMode = PostView.ViewMode.MSG
                msgRes = R.string.error_unexpected
                view?.switchMode(mMode)
                view?.showMessage(msgRes)
                t.printStackTrace()
            }}
        )
    }

    override fun onDestroy() {
        dispose (mDisposable)
        super.onDestroy()
    }


}