package com.test.reddit.app.base

import androidx.annotation.StringRes

interface IView {
    fun showMessage (@StringRes resId: Int)
    fun showMessage (msg: String)
}