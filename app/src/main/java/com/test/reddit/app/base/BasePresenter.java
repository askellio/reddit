package com.test.reddit.app.base;

import io.reactivex.disposables.Disposable;
import org.jetbrains.annotations.NotNull;

public abstract class BasePresenter<V extends IView>
        implements IPresenter<V> {

    protected V view;
    private boolean mFirstRun = true;

    @Override
    public void attachView(@NotNull V view) {
        this.view = view;
        if (mFirstRun) {
            mFirstRun = false;
            doOnFirstRun();
        } else
            doOnOtherRun();
    }

    @Override
    public void detachView() {
        view = null;
    }

    @Override
    public void onDestroy() {

    }

    protected void dispose (Disposable disposable) {
        if (disposable != null) {
            disposable.dispose();
        }
    }
}
