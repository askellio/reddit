package com.test.reddit.app.base

interface IPresenter<V: IView> {
    fun attachView (view: V)
    fun detachView ()
    fun onDestroy()
    fun doOnFirstRun()
    fun doOnOtherRun()
}