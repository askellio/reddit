package com.test.reddit.app.post

import android.os.Bundle
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.reddit.R
import com.test.reddit.app.base.BaseFragment
import com.test.reddit.app.base.BasePresenter
import com.test.reddit.domain.model.Post
import kotlinx.android.synthetic.main.fragment_post.*
import android.R.menu
import android.view.MenuInflater



class PostFragment:
    BaseFragment<PostPresenter>(),
    PostView {

    private lateinit var adapter: PostAdapter

    override fun createPresenter(): PostPresenter = PostPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate (
            R.layout.fragment_post,
            container,
            false
        )
    }

    override fun setup(view: View?) {
        adapter = PostAdapter()
        rvPost.layoutManager = LinearLayoutManager(context)
        rvPost.adapter = adapter

        btnPostRetry.setOnClickListener { mPresenter.onRetry() }

        setHasOptionsMenu (true)
    }

    override fun onDestroyView() {
        btnPostRetry.setOnClickListener (null)
        super.onDestroyView()
    }

    override fun showMessage(resId: Int) {
        showMessage (getString (resId))
    }

    override fun showMessage(msg: String) {
        tvPostMsg.setText (msg)
    }

    override fun showData(posts: List<Post>) {
        adapter.update (posts)
    }

    override fun switchMode(mode: PostView.ViewMode) {
        val views = listOf<View> (tvPostMsg, btnPostRetry, rvPost, pbPost)
        views.forEach { it.visibility = GONE }
        when (mode) {
            PostView.ViewMode.LOADING ->
                pbPost.visibility = VISIBLE
            PostView.ViewMode.MAIN ->
                rvPost.visibility = VISIBLE
            PostView.ViewMode.MSG -> run {
                btnPostRetry.visibility = VISIBLE
                tvPostMsg.visibility = VISIBLE
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu,
                                     inflater: MenuInflater) {
        inflater.inflate (
            com.test.reddit.R.menu.menu_post,
            menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuPostRetry -> mPresenter.onUpdate()
        }
        return true
    }
}