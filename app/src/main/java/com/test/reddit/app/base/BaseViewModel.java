package com.test.reddit.app.base;

import androidx.lifecycle.ViewModel;

public class BaseViewModel<P extends IPresenter>
        extends ViewModel {

    private P mPresenter = null;

    P getPresenter() {
        return mPresenter;
    }

    void setPresenter(P presenter) {
        this.mPresenter = presenter;
    }
}
