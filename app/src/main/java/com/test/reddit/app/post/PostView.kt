package com.test.reddit.app.post

import com.test.reddit.app.base.IView
import com.test.reddit.domain.model.Post

interface PostView: IView {

    fun showData (posts: List<Post>)
    fun switchMode (mode: ViewMode)

    enum class ViewMode {
        LOADING,
        MAIN,
        MSG
    }
}