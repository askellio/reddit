package com.test.reddit.app.post

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.test.reddit.R
import com.test.reddit.domain.model.Post
import kotlinx.android.synthetic.main.item_post.view.*

class PostAdapter:
    RecyclerView.Adapter<PostAdapter.Vh>() {

    private var posts: List<Post> = listOf()

    public fun update (posts: List<Post>) {
        this.posts = posts
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): Vh {
        return Vh (
            LayoutInflater.from (parent.context)
                .inflate (
                    R.layout.item_post,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount(): Int = posts.size

    override fun onBindViewHolder(holder: Vh,
                                  pos: Int) {
        val context = holder?.itemView.context
        holder?.itemView?.tvPostItemAuthor.setText (
            context.getString (
                R.string.posted,
                posts.get(pos).author))
        holder?.itemView?.tvPostItemTitle.setText (
                posts.get(pos).title)
        Glide.with (context)
            .load (posts.get(pos).url)
            .into (holder?.itemView?.ivPostItem)
    }

    class Vh (view: View):
        RecyclerView.ViewHolder(view) {
    }
}