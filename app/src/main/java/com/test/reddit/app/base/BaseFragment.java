package com.test.reddit.app.base;

import android.os.Bundle;
import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseFragment<P extends IPresenter>
        extends Fragment
        implements IView {
    protected P mPresenter;
    private String mRetainSuffix = " Retain";
    protected String mRetainTag = getClass().getSimpleName()+mRetainSuffix;

    @Override
    public void onViewCreated (@NonNull View view,
                               Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseViewModel<P> model = ViewModelProviders.of(this)
                .get(mRetainTag, BaseViewModel.class);
        mPresenter = model.getPresenter();
        if (mPresenter == null) {
            mPresenter = createPresenter();
            model.setPresenter(mPresenter);
        }
        setup (view);
        if (mPresenter != null) {
            mPresenter.attachView (this);
        }
    }

    public abstract P createPresenter();
    public abstract void setup (View view);

    @CallSuper
    @Override
    public void onDestroyView() {
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (isRemoving() &&
            mPresenter != null)
            mPresenter.onDestroy();
        super.onDestroy();

    }
}
