package com.test.reddit.domain

interface IMapper<In, Out> {
    fun map (obj: In): Out
}