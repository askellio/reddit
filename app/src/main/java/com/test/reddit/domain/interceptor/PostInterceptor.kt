package com.test.reddit.domain.interceptor

import com.test.reddit.RedditApp
import com.test.reddit.domain.IRepository
import com.test.reddit.domain.mapper.PostMapper
import com.test.reddit.domain.model.Post
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PostInterceptor {

    @Inject lateinit var repository: IRepository

    init {
        RedditApp.domainComponent.inject(this)
    }

    fun posts (connected: Boolean): Single<List<Post>> =
            repository.posts (connected)
                .flatMap { data -> Single.just (
                    data.map {
                        PostMapper.map(it)
                    }) }
                .subscribeOn (Schedulers.io())
                .observeOn (AndroidSchedulers.mainThread())
}