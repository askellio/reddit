package com.test.reddit.domain.mapper

import com.test.reddit.data.model.entity.DataPost
import com.test.reddit.domain.IMapper
import com.test.reddit.domain.model.Post

class PostMapper {
    companion object: IMapper<DataPost, Post> {
        override fun map(obj: DataPost): Post =
            Post(
                obj.title,
                obj.author,
                obj.url
            )
    }
}