package com.test.reddit.domain.model

data class Post (var title: String,
                 var author: String,
                 var url: String) {
}