package com.test.reddit.domain

import com.test.reddit.data.model.entity.DataPost
import io.reactivex.Single

interface IRepository {
    fun posts (connected: Boolean): Single<List<DataPost>>
}