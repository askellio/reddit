package com.test.reddit.data.model

class Wrapper<T> (var data: DataObject<T>) {
}

class DataObject<T> (var children: List<ChildObject<T>>) {}

class ChildObject<T> (var data: T) {
}