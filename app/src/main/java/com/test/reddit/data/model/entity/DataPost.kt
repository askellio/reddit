package com.test.reddit.data.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DataPost (var title: String,
                     var author: String,
                     var url: String) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}