package com.test.reddit.data

import com.test.reddit.LIMIT_COUNT
import com.test.reddit.RedditApp
import com.test.reddit.data.dao.PostDao
import com.test.reddit.data.mapper.DataPostMapper
import com.test.reddit.data.mapper.DataPostMapper.Companion.map
import com.test.reddit.data.model.ResponsePost
import com.test.reddit.data.model.entity.DataPost
import com.test.reddit.domain.IRepository
import io.reactivex.Single
import javax.inject.Inject

class Repository : IRepository {

    @Inject lateinit var postDao: PostDao
    @Inject lateinit var httpService: HttpService

    init {
        RedditApp.dataComponent.inject(this)
    }

    override fun posts (connected: Boolean): Single<List<DataPost>> {
        val result: Single<List<DataPost>>
        if (connected)
            result = httpService.posts (LIMIT_COUNT)
                .map { response -> response.data.children }
                .toObservable ()
                .flatMapIterable { children -> children }
                .map { child -> child.data }
                .map { responsePost ->  DataPostMapper.map (responsePost) }
                .toList()
                .doOnSuccess { posts -> postDao.updateAll (posts) }
                .doOnError { t -> postDao.all() }
        else
            result = postDao.all()

        return result
    }
}