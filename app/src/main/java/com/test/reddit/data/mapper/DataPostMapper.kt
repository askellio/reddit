package com.test.reddit.data.mapper

import com.test.reddit.data.model.entity.DataPost
import com.test.reddit.data.model.ResponsePost
import com.test.reddit.domain.IMapper

class DataPostMapper {
    companion object: IMapper<ResponsePost, DataPost>  {
        override fun map(obj: ResponsePost): DataPost =
            DataPost(
                obj.title,
                obj.author,
                obj.url
            )
    }
}