package com.test.reddit.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.test.reddit.data.dao.PostDao
import com.test.reddit.data.model.entity.DataPost

@Database (entities = [
    DataPost::class
], version = 1)
abstract class Database: RoomDatabase() {
    abstract fun postDao (): PostDao
}