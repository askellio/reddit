package com.test.reddit.data.dao

import androidx.room.*
import com.test.reddit.data.model.entity.DataPost
import io.reactivex.Single

@Dao
interface PostDao {
    @Insert
    fun insert (item: DataPost): Long

    @Insert
    fun insert (items: List<DataPost>): LongArray

    @Delete
    fun delete (item: DataPost): Int

    @Query("DELETE FROM DataPost;")
    fun deleteAll (): Int

    @Update
    fun update (item: DataPost): Int

    @Transaction
    fun updateAll (items: List<DataPost>) {
        if (!items.isEmpty()) {
            deleteAll()
            insert (items)
        }
    }

    @Query("SELECT * FROM DataPost;")
    fun all(): Single<List<DataPost>>
}