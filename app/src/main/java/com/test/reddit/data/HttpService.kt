package com.test.reddit.data

import com.test.reddit.data.model.ResponsePost
import com.test.reddit.data.model.Wrapper
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface HttpService {

    @GET("r/bmw/top.json")
    fun posts (@Query("limit") limit: Int): Single<Wrapper<ResponsePost>>
}