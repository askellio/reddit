package com.test.reddit.data.model

data class ResponsePost (var title: String,
                         var author: String,
                         var url: String) {
}