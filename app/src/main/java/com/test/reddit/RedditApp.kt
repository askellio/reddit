package com.test.reddit

import android.app.Application
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher
import com.test.reddit.di.component.*
import com.test.reddit.di.module.*
import io.reactivex.plugins.RxJavaPlugins

class RedditApp: Application() {
    companion object {
        lateinit var dataComponent: DataComponent
        lateinit var domainComponent: DomainComponent
        lateinit var appComponent: AppComponent
        lateinit var mRefWatcher: RefWatcher
    }


    override fun onCreate() {
        super.onCreate()

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }

        mRefWatcher = LeakCanary.install(this)

        dataComponent = DaggerDataComponent.builder()
            .appModule (
                AppModule(this))
            .netModule (
                NetModule (SERVER_URL))
            .dbModule (
                DbModule (DB_NAME))
            .build()

        domainComponent = DaggerDomainComponent.builder()
            .repoModule (RepoModule())
            .build()

        appComponent = DaggerAppComponent.builder()
            .appModule (AppModule(this))
            .interceptorModule (InterceptorModule())
            .connectivityModule (ConnectivityModule())
            .build()

        RxJavaPlugins.setErrorHandler {
            t: Throwable? -> t?.printStackTrace()
        }
    }
}