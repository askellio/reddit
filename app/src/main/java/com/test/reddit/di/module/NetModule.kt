package com.test.reddit.di.module

import android.app.Application
import androidx.room.Room
import com.test.reddit.data.Database
import com.test.reddit.data.HttpService
import com.test.reddit.data.dao.PostDao
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetModule (val serverUrl: String) {

    @Singleton
    @Provides
    fun provideHttpService (): HttpService =
        Retrofit.Builder()
            .client (OkHttpClient())
            .addCallAdapterFactory (RxJava2CallAdapterFactory.create())
            .addConverterFactory (GsonConverterFactory.create())
            .baseUrl (serverUrl)
            .build()
            .create (HttpService::class.java)
}