package com.test.reddit.di.module

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule (val app: Application) {

    @Singleton
    @Provides
    fun provideApp () = app
}