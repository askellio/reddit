package com.test.reddit.di.component

import com.test.reddit.app.post.PostPresenter
import com.test.reddit.di.module.AppModule
import com.test.reddit.di.module.ConnectivityModule
import com.test.reddit.di.module.InterceptorModule
import dagger.Component
import dagger.Module
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    InterceptorModule::class,
    ConnectivityModule::class
])
interface AppComponent {
    fun inject (presenter: PostPresenter)
}