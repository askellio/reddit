package com.test.reddit.di.module

import android.app.Application
import androidx.room.Room
import com.test.reddit.data.Database
import com.test.reddit.data.dao.PostDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule (val dbName: String) {

    @Singleton
    @Provides
    fun provideDatabase (app: Application): Database =
        Room.databaseBuilder (
            app.applicationContext,
            Database::class.java,
            dbName
        ).build()

    @Singleton
    @Provides
    fun providePostDao (db: Database): PostDao = db.postDao()
}