package com.test.reddit.di.component

import com.test.reddit.data.Repository
import com.test.reddit.di.module.RepoModule
import com.test.reddit.domain.interceptor.PostInterceptor
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component (modules = [
    RepoModule::class
])
interface DomainComponent {
    fun inject (interceptor: PostInterceptor)
}