package com.test.reddit.di.module

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import androidx.core.content.getSystemService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ConnectivityModule () {

    @Singleton
    @Provides
    fun provideConnectivity (app: Application): ConnectivityManager {
        return app.applicationContext
            .getSystemService (Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
    }
}