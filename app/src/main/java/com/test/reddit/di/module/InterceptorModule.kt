package com.test.reddit.di.module

import com.test.reddit.domain.interceptor.PostInterceptor
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class InterceptorModule {

    @Singleton
    @Provides
    fun providePostInterceptor (): PostInterceptor = PostInterceptor()
}