package com.test.reddit.di.component

import com.test.reddit.data.Repository
import com.test.reddit.di.module.AppModule
import com.test.reddit.di.module.DbModule
import com.test.reddit.di.module.NetModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component (modules = [
    AppModule::class,
    NetModule::class,
    DbModule::class
])
interface DataComponent {
    fun inject (repo: Repository)
}