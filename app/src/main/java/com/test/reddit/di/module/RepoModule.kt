package com.test.reddit.di.module

import com.test.reddit.data.Repository
import com.test.reddit.domain.IRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepoModule {
    @Singleton
    @Provides
    fun provideRepo (): IRepository = Repository()
}